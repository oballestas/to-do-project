## Dependencies

* [pipenv](https://pipenv.pypa.io/en/latest/install/#installing-pipenv)

## Installation

1. `pipenv install`
2. `pipenv shell`